#include "MyPathfinding.hpp"
#include "MyUnitActionApply.hpp"
#include "MySimplifiedUnit.hpp"
#include "MyMath.h"

#include <queue>

static const double mtileFactor = 3;
static int usedAtGlobal = 0;
static std::vector<PosTimeAction> mParent(200 * 200 * 6);
static std::vector<PosAndJumpTime> mMinAtPoint(200 * 200);

static Vec2Int coordToTile(const Vec2Double& coord) {
   return Vec2Int(static_cast<int>(coord.x), static_cast<int>(coord.y));
}

static Vec2Int coordToMicrotile(const Vec2Double& coord) {
   return Vec2Int(static_cast<int>(coord.x * mtileFactor), static_cast<int>(coord.y * mtileFactor));
}

static PosAndJumpTime unit2PosTime(const MySimplifiedUnit& unit, int iteration) {
   return { coordToMicrotile(unit.position), static_cast<int>(unit.jumpState.maxTime * 10), usedAtGlobal, iteration };
}

static bool pointInsideUnit(const Unit& u, const Vec2Double& point) {
   const double real_x1 = u.position.x - u.halfSizeX;
   const double real_x2 = u.position.x + u.halfSizeX;
   const double real_y1 = u.position.y;
   const double real_y2 = u.position.y + u.size.y;

   return point.x >= real_x1 && point.x <= real_x2
      && point.y >= real_y1 && point.y <= real_y2;
}

static std::array<UnitAction, 8> actions = {
   UnitAction(-10, true, false, Vec2Double(0, 0), false, false, false, false),
   UnitAction(10, true, false, Vec2Double(0, 0), false, false, false, false),
   UnitAction(10, false, true, Vec2Double(0, 0), false, false, false, false),
   UnitAction(-10, false, true, Vec2Double(0, 0), false, false, false, false),
   UnitAction(0, true, false, Vec2Double(0, 0), false, false, false, false),
   UnitAction(0, false, true, Vec2Double(0, 0), false, false, false, false),
   UnitAction(-10, false, false, Vec2Double(0, 0), false, false, false, false),
   UnitAction(10, false, false, Vec2Double(0, 0), false, false, false, false)
};

MyPathfinding::MyPathfinding(const Unit& unit, const Game& game, Debug& debug) {
   ++usedAtGlobal; // not thread safe!
   std::queue<std::pair<MySimplifiedUnit, int>> bfsq;
   const auto start = unit2PosTime(unit, 0);
   bfsq.push({ unit, 0 });
   parentAt(start) = { start, UnitAction(-42, false, false, Vec2Double(0, 0), false, false, false, false) };
   minpAt(start.microtilePos) = start;
   const double tickFactor = 1 / game.properties.ticksPerSecond;

   while (!bfsq.empty()) {
      const auto current = bfsq.front();
      const auto currentPostime = unit2PosTime(current.first, current.second);
      bfsq.pop();

      const unsigned int ticksPassed = current.second + 1;
      for (const auto& action : actions) {
         const auto appl = applyActionMovements(current.first, action, game, debug); // 2times, 2microticks
         const auto postime = unit2PosTime(appl, ticksPassed);

         if (parentAt(postime).mtPostime.usedAt == usedAtGlobal) {
            continue;
         }

         {
            const double real_x1 = appl.position.x - unit.halfSizeX;
            const double real_x2 = appl.position.x + unit.halfSizeX;
            const double real_y1 = appl.position.y;
            const double real_y2 = appl.position.y + unit.size.y;

            bool shot = false;
            for (int i = 0; i < 3 && !shot; ++i) {
               for (const auto& bullet : game.bullets) {
                  auto bulletPos = bullet.position;
                  bulletPos.x += bullet.velocity.x * ticksPassed * 2 * tickFactor;
                  bulletPos.y += bullet.velocity.y * ticksPassed * 2 * tickFactor;
                  bulletPos.x -= bullet.velocity.x * tickFactor * i; // backtrace
                  bulletPos.y -= bullet.velocity.y * tickFactor * i; // backtrace
                  double size = bullet.size;
                  double halfSize = bullet.size / 2;

                  bool collidesUnit = false;
                  const auto bulletlx = bulletPos.x - halfSize;
                  const auto bulletrx = bulletPos.x + halfSize;
                  const auto bulletly = bulletPos.y - halfSize;
                  const auto bulletry = bulletPos.y + halfSize;
                  if (WeaponType::ROCKET_LAUNCHER == bullet.weaponType) {
                     for (const auto& oth : game.units) {
                        if (unit.id == oth.id) {
                           continue;
                        }

                        if (pointInsideUnit(oth, Vec2Double(bulletlx, bulletly))
                           || pointInsideUnit(oth, Vec2Double(bulletlx, bulletry))
                           || pointInsideUnit(oth, Vec2Double(bulletrx, bulletly))
                           || pointInsideUnit(oth, Vec2Double(bulletrx, bulletry))) {
                           collidesUnit = true;
                           break;
                        }
                     }
                  }

                  if (WeaponType::ROCKET_LAUNCHER == bullet.weaponType
                     && bulletPos.x >= 0 && bulletPos.x <= game.level.tiles.size()
                     && bulletPos.y >= 0 && bulletPos.y <= game.level.tiles[0].size()
                     && (collidesUnit
                        || Tile::WALL == game.level.tiles[static_cast<unsigned int>(bulletlx)][static_cast<unsigned int>(bulletly)]
                        || Tile::WALL == game.level.tiles[static_cast<unsigned int>(bulletlx)][static_cast<unsigned int>(bulletry)]
                        || Tile::WALL == game.level.tiles[static_cast<unsigned int>(bulletrx)][static_cast<unsigned int>(bulletly)]
                        || Tile::WALL == game.level.tiles[static_cast<unsigned int>(bulletrx)][static_cast<unsigned int>(bulletry)])) {
                     size = game.properties.weaponParams.at(WeaponType::ROCKET_LAUNCHER).explosion->radius;

                     // debug.draw(CustomData::Rect(Vec2Float(bulletPos.x - size, bulletPos.y - size), Vec2Float(size * 2, size * 2), ColorFloat(1, 1, 1, 0.01)));
                     // todo: xm, sometimes booms

                     if ((real_x1 <= bulletPos.x + size && real_x1 >= bulletPos.x - size
                        && real_y1 <= bulletPos.y + size && real_y1 >= bulletPos.y - size)
                        || (real_x1 <= bulletPos.x + size && real_x1 >= bulletPos.x - size
                           && real_y2 <= bulletPos.y + size && real_y2 >= bulletPos.y - size)
                        || (real_x2 <= bulletPos.x + size && real_x2 >= bulletPos.x - size
                           && real_y1 <= bulletPos.y + size && real_y1 >= bulletPos.y - size)
                        || (real_x2 <= bulletPos.x + size && real_x2 >= bulletPos.x - size
                           && real_y2 <= bulletPos.y + size && real_y2 >= bulletPos.y - size)) {
                        shot = true;
                        break;
                     }
                  }
                  else if (bulletPos.x + size >= real_x1 && bulletPos.x - size <= real_x2
                     && bulletPos.y + size >= real_y1 && bulletPos.y - size <= real_y2) {
                     shot = true;
                     break;
                  }
                  {
                     //const auto bp = coordToMicrotile(bulletPos);
                     //debug.draw(CustomData::Rect(Vec2Float(bp.x / mtileFactor, bp.y / mtileFactor),
                     //   Vec2Float(1 / mtileFactor, 1 / mtileFactor), ColorFloat(0, 1, 0, 0.3)));
                  }
               }
            }

            if (shot) {
               //debug.draw(CustomData::Rect(Vec2Float(postime.microtilePos.x / mtileFactor, postime.microtilePos.y / mtileFactor),
               //   Vec2Float(1 / mtileFactor, 1 / mtileFactor), ColorFloat(1, 0, 0, 0.3)));
               continue;
            }
         }

         parentAt(postime) = { currentPostime, action };
         if (minpAt(postime.microtilePos).usedAt != usedAtGlobal) {
            minpAt(postime.microtilePos) = postime;
         }
         //debug.draw(CustomData::Rect(Vec2Float(postime.microtilePos.x / mtileFactor, postime.microtilePos.y / mtileFactor),
         //   Vec2Float(1 / mtileFactor, 1 / mtileFactor), ColorFloat(0, 1, 1, 0.3)));
         bfsq.push({ appl, postime.iteration });
      }
   }
}

std::optional<UnitAction> MyPathfinding::firstActionOnPathTo(const Vec2Int& tile, Debug& debug) {
   std::optional<Vec2Int> minMtile;
   int mitime = static_cast<int>(1e9);
   const auto mtileFactorInt = static_cast<unsigned int>(mtileFactor);
   for (unsigned int microtileX = tile.x * mtileFactorInt, i = 0; i < mtileFactorInt - 1; ++i, ++microtileX) {
      for (unsigned int microtileY = tile.y * mtileFactorInt + 1, j = 0; j < mtileFactorInt - 1; ++j, ++microtileY) {
         auto asvec2int = Vec2Int(microtileX, microtileY);
         if (minpAt(asvec2int).usedAt == usedAtGlobal) { // select minimum
            if (mitime > minpAt(asvec2int).iteration) {
               mitime = minpAt(asvec2int).iteration;
               minMtile = asvec2int;
            }
         }
      }
   }

   if (minMtile.has_value()) {
      auto postime = parentAt(minpAt(minMtile.value())).mtPostime;
      UnitAction lasta;
      do {
         const auto pr = parentAt(postime);
         postime = pr.mtPostime;
         lasta = pr.action;
         //ColorFloat clr;
         //if (lasta.jump) {
         //   clr = ColorFloat(0, 1, 0, 0.2);
         //}
         //else if (lasta.jumpDown) {
         //   clr = ColorFloat(0, 0, 1, 0.2);
         //}
         //else {
         //   clr = ColorFloat(1, 1, 1, 0.2);
         //}
         //debug.draw(CustomData::Rect(vecD2F({ postime.microtilePos.x / mtileFactor, postime.microtilePos.y / mtileFactor }),
         //   Vec2Float(1 / mtileFactor, 1 / mtileFactor), clr));
      } while (parentAt(postime).action.velocity != -42); // start pt
      return lasta;
   }

   return std::nullopt;
}

std::optional<UnitAction> MyPathfinding::firstActionOnPathTo(const Vec2Double& position, Debug& debug) {
   return firstActionOnPathTo(coordToTile(position), debug);
}

int MyPathfinding::timeTo(const Vec2Double& position, Debug& debug) {
   int mitime = static_cast<int>(1e9);
   const auto tile = coordToTile(position);
   const auto mtileFactorInt = static_cast<unsigned int>(mtileFactor);
   for (unsigned int microtileX = tile.x * mtileFactorInt, i = 0; i < mtileFactorInt - 1; ++i, ++microtileX) {
      for (unsigned int microtileY = tile.y * mtileFactorInt + 1, j = 0; j < mtileFactorInt - 1; ++j, ++microtileY) {
         auto asvec2int = Vec2Int(microtileX, microtileY);
         if (minpAt(asvec2int).usedAt == usedAtGlobal) { // select minimum
            if (mitime > minpAt(asvec2int).iteration) {
               mitime = minpAt(asvec2int).iteration;
            }
         }
      }
   }

   return mitime;
}

PosTimeAction& MyPathfinding::parentAt(const PosAndJumpTime& inp) {
   return mParent[inp.microtilePos.x * 200LL * 6 + inp.microtilePos.y * 6LL + inp.jumptime];
}

PosAndJumpTime& MyPathfinding::minpAt(const Vec2Int& inp) {
   return mMinAtPoint[inp.x * 200LL + inp.y];
}
