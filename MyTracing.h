#pragma once
#include "model/Unit.hpp"
#include "model/Game.hpp"
#include "Debug.hpp"
#include "MyMath.h"
#include "Vec2Int.hpp"

// bullet size 0.4

static bool canShootToUnit(const Unit& unit, const Game& game, Debug& debug, const Unit& target) {
   if (!unit.weapon || !unit.weapon->lastAngle) {
      return false;
   }

   Vec2Double startPt = unit.position;
   startPt.y += unit.size.y / 2; // center

   Vec2Double finPt = target.position;
   finPt.y += target.size.y / 2; // center

   Vec2Double vectorAim = {finPt.x - startPt.x, finPt.y - startPt.y};
   const double vectorAimLen = vectorLen(vectorAim);
   const unsigned int factor = 5;
   { // norm
      const auto veclen5 = vectorAimLen * factor;
      vectorAim.x /= veclen5;
      vectorAim.y /= veclen5;
   }

   {
      const double upAngle = *(unit.weapon->lastAngle) + unit.weapon->spread;
      const double downAngle = *(unit.weapon->lastAngle) - unit.weapon->spread;
      const double dist = std::min(vectorAimLen - unit.halfSizeX, unit.weapon->typ == WeaponType::ROCKET_LAUNCHER ? 2.5 : 1.0);
      const double bulletRadius = sqrt(distanceSqr(
         Vec2Double(startPt.x + dist * cos(upAngle), startPt.y + dist * sin(upAngle)),
         Vec2Double(startPt.x + dist * cos(downAngle), startPt.y + dist * sin(downAngle)))) / 2;
      //debug.draw(CustomData::Polygon({ ColoredVertex(Vec2Float(startPt.x, startPt.y), ColorFloat(1, 1, 0, 0.4)),
      //   ColoredVertex(Vec2Float(startPt.x + dist * cos(upAngle), startPt.y + dist * sin(upAngle)), ColorFloat(1, 1, 0, 0.4)),
      //   ColoredVertex(Vec2Float(startPt.x + dist * cos(downAngle), startPt.y + dist * sin(downAngle)), ColorFloat(1, 1, 0, 0.4)) }));
      Vec2Double tracingPoint = startPt;
      const int tracingCount = static_cast<int>(vectorAimLen) * factor;
      for (int i = 0; i < tracingCount; ++i) {
         const Vec2Int tile1(static_cast<int>(tracingPoint.x - bulletRadius), static_cast<int>(tracingPoint.y + bulletRadius));
         const Vec2Int tile2(static_cast<int>(tracingPoint.x - bulletRadius), static_cast<int>(tracingPoint.y - bulletRadius));
         const Vec2Int tile3(static_cast<int>(tracingPoint.x + bulletRadius), static_cast<int>(tracingPoint.y + bulletRadius));
         const Vec2Int tile4(static_cast<int>(tracingPoint.x + bulletRadius), static_cast<int>(tracingPoint.y - bulletRadius));

         //debug.draw(CustomData::Rect(Vec2Float(tileL.x, tileL.y), Vec2Float(1,1), ColorFloat(0, 0, 1, 0.6)));
         //debug.draw(CustomData::Rect(Vec2Float(tileU.x, tileU.y), Vec2Float(1,1), ColorFloat(0, 0, 1, 0.6)));

         if (Tile::WALL == game.level.tiles[tile1.x][tile1.y]
            || Tile::WALL == game.level.tiles[tile2.x][tile2.y]
            || Tile::WALL == game.level.tiles[tile3.x][tile3.y]
            || Tile::WALL == game.level.tiles[tile4.x][tile4.y]) {
            return false;
         }

         {
            const double real_x1 = tracingPoint.x - bulletRadius;
            const double real_x2 = tracingPoint.x + bulletRadius;
            const double real_y1 = tracingPoint.y - bulletRadius;
            const double real_y2 = tracingPoint.y + bulletRadius;

            for (const auto& oup : game.units) {
               if (oup.playerId != unit.playerId || oup.id == unit.id) {
                  continue;
               }
               const double oup_x1 = oup.position.x - unit.halfSizeX;
               const double oup_x2 = oup.position.x + unit.halfSizeX;
               const double oup_y1 = oup.position.y;
               const double oup_y2 = oup.position.y + unit.size.y;

               if ((real_x1 <= oup_x2 && real_x1 >= oup_x1
                  && real_y1 <= oup_y2 && real_y1 >= oup_y1)
                  || (real_x1 <= oup_x2 && real_x1 >= oup_x1
                     && real_y2 <= oup_y2 && real_y2 >= oup_y1)
                  || (real_x2 <= oup_x2 && real_x2 >= oup_x1
                     && real_y1 <= oup_y2 && real_y1 >= oup_y1)
                  || (real_x2 <= oup_x2 && real_x2 >= oup_x1
                     && real_y2 <= oup_y2 && real_y2 >= oup_y1)) {
                  return false;
               }

               if ((oup_x1 <= real_x2 && oup_x1 >= real_x1
                  && oup_y1 <= real_y2 && oup_y1 >= real_y1)
                  || (oup_x1 <= real_x2 && oup_x1 >= real_x1
                     && oup_y2 <= real_y2 && oup_y2 >= real_y1)
                  || (oup_x2 <= real_x2 && oup_x2 >= real_x1
                     && oup_y1 <= real_y2 && oup_y1 >= real_y1)
                  || (oup_x2 <= real_x2 && oup_x2 >= real_x1
                     && oup_y2 <= real_y2 && oup_y2 >= real_y1)) {
                  return false;
               }
            }
         }

         tracingPoint.x += vectorAim.x;
         tracingPoint.y += vectorAim.y;
         // debug.draw(CustomData::Rect(Vec2Float(real_x1, real_y1), Vec2Float(bulletRadius * 2, bulletRadius * 2), ColorFloat(1, 1, 0, 0.2)));
      }
   }

   // todo: check cone
   //std::vector<Vec2Double> enemyAngles = {
   //   {target.position.x - target.halfSizeX, target.position.y},
   //   {target.position.x - target.halfSizeX, target.position.y + target.size.y},
   //   {target.position.x + target.halfSizeX, target.position.y},
   //   {target.position.x + target.halfSizeX, target.position.y + target.size.y}
   //};

   // calc all conuses, select with max area
   // d = max(d_to_pt_A, d_to_pt_B)
   // dC = polar(d, angle_to_pt_A)
   // dD = polar(d, angle_to_pt_B)
   // conusA = (start, dC, dD)
   // ptZ = intersect lines (dC, dD) and (start, polar(aim_angle - spread, 1))
   // ptW = intersect lines (dC, dD) and (start, polar(aim_angle + spread, 1))
   // conusB = (start, ptZ, ptW)

   return true;
}
