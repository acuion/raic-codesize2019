#include "MyStrategy.hpp"
#include "MyMath.h"
#include "Vec2Int.hpp"
#include "MyPathfinding.hpp"
#include "MyTracing.h"
#include "MyUnitActionApply.hpp"

#include <algorithm>
#include <chrono>
#include <iostream>

static Vec2Double initPosition;
static std::vector<Vec2Double> lastUnitPos(10, Vec2Double(-1, -1));

static bool alreadyCalled = false;
static void init(const Unit& unit, const Game& game, Debug& debug) {
   initPosition = unit.position;

   if (alreadyCalled) {
      return;
   }
   alreadyCalled = true;

   precomputePathfind(game);
   std::ios_base::sync_with_stdio(false);
}

// find closest aidkit, weapon, enemy

static const Unit* nearestEnemy(const Unit& unit, MyPathfinding& pf, const Game& game, Debug& debug) {
   const Unit* nearestEnemy = nullptr;
   double maxDist = 1e9;
   double currDist = 1e9;
   std::vector<const Unit*> shootable;
   const Vec2Double unitCenter(unit.position.x, unit.position.y + unit.size.y / 2);
   for (const Unit& other : game.units) {
      if (other.playerId != unit.playerId) {
         if (canShootToUnit(unit, game, debug, other)) {
            shootable.push_back(&other);
         }
         currDist = distanceSqr(unitCenter, other.position);
         if (currDist < maxDist) {
            maxDist = currDist;
            nearestEnemy = &other;
         }
      }
   }

   if (shootable.size()) {
      if (shootable.size() > 1) {
         sort(shootable.begin(), shootable.end(), [&](const Unit* a, const Unit* b) {
            if (a->health != b->health)
               return a->health < b->health;
            return distanceSqr(unitCenter, a->position) < distanceSqr(unitCenter, b->position);
         });
      }
      return shootable[0];
   }
   return nearestEnemy;
}

static const LootBox* nearestWeapon(const Unit& unit, MyPathfinding& pf, const Game& game, Debug& debug) {
   const LootBox* nearestLb = nullptr;
   double maxDist = 1e9;
   double currDist = 1e9;
   for (const LootBox& lootBox : game.lootBoxes) {
      if (std::dynamic_pointer_cast<Item::Weapon>(lootBox.item)) {
         currDist = pf.timeTo(lootBox.position, debug);
         if (currDist < maxDist) {
            maxDist = currDist;
            nearestLb = &lootBox;
         }
      }
   }
   return nearestLb;
}

static const LootBox* nearestHeal(const Unit& unit, MyPathfinding& pf, const Game& game, Debug& debug) {
   const LootBox* nearestLb = nullptr;
   double maxDist = 1e9;
   double currDist = 1e9;
   for (const LootBox& lootBox : game.lootBoxes) {
      if (std::dynamic_pointer_cast<Item::HealthPack>(lootBox.item)) {
         currDist = pf.timeTo(lootBox.position, debug);
         if (currDist < maxDist) {
            maxDist = currDist;
            nearestLb = &lootBox;
         }
      }
   }
   return nearestLb;
}

// jump when trapped
const auto emptyAction = UnitAction(0, true, false, Vec2Double(1, 1), false, false, false, false);
static UnitAction lastMileToLootbox(const Unit& unit, const Game& game, const LootBox* lootbox) {
   auto result = emptyAction;
   if (unit.position.x < lootbox->position.x) {
      result.velocity = 10;
   }
   else {
      result.velocity = -10;
   }

   if (unit.position.y < lootbox->position.y) {
      result.jump = true;
   }
   else {
      result.jump = false;
      result.jumpDown = true;
   }

   return result;
}

static std::optional<UnitAction> gotoLootbox(const Unit& unit, MyPathfinding& pf, const Game& game, Debug& debug, const LootBox* lootbox) {
   const double wpdist = distanceSqr(unit.position, lootbox->position);
   if (wpdist < 1.5) { // <1.5 tile
      return lastMileToLootbox(unit, game, lootbox);
   }
   else {
      return pf.firstActionOnPathTo(lootbox->position, debug);
   }
}

UnitAction MyStrategy::getAction(const Unit& unit, const Game& game, Debug& debug) {
   //std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
   if (0 == game.currentTick) {
      init(unit, game, debug);
   }

   UnitAction currentAction = emptyAction;
   auto paths = MyPathfinding(unit, game, debug);

   const auto nrEnemy = nearestEnemy(unit, paths, game, debug);
   const auto nrWeapon = nearestWeapon(unit, paths, game, debug);
   const auto nrHeal = nearestHeal(unit, paths, game, debug);

   if (!unit.weapon && nrWeapon) {
      const auto candidate = gotoLootbox(unit, paths, game, debug, nrWeapon);
      if (candidate.has_value()) {
         currentAction = candidate.value();
      }
   }
   else if (unit.health < game.properties.unitMaxHealth * 0.7 && nrHeal) {
      const auto candidate = gotoLootbox(unit, paths, game, debug, nrHeal);
      if (candidate.has_value()) {
         currentAction = candidate.value();
      }
   }
   else if (nrEnemy) {
      const Vec2Double enemyCenter = { nrEnemy->position.x , nrEnemy->position.y + nrEnemy->size.y / 2 };
      const Vec2Double unitCenter = { unit.position.x, unit.position.y + unit.size.y / 2 };
      const double dst = distanceSqr(enemyCenter, unitCenter);
      bool goBack = false;
      if (dst > 21) {
         const auto candidate = paths.firstActionOnPathTo(nrEnemy->position, debug);
         if (candidate.has_value()) {
            currentAction = candidate.value();
         }
         else {
            goBack = true;
         }
      }
      else {
         goBack = true;
      }

      if (goBack) {
         const auto candidate = paths.firstActionOnPathTo(initPosition, debug);
         if (candidate.has_value()) {
            currentAction = candidate.value();
         }
      }
   }

   if (nrEnemy) {
      const double distToEnemy = distanceSqr(nrEnemy->position, unit.position);
      double dx = 0, dy = 0;
      if (lastUnitPos[nrEnemy->id].x > -1 && distToEnemy > 10) {
         dx = nrEnemy->position.x - lastUnitPos[nrEnemy->id].x;
         dy = nrEnemy->position.y - lastUnitPos[nrEnemy->id].y;
         dx *= 2;
         dy *= 2;
      }

      const Vec2Double enemyCenter = { nrEnemy->position.x + dx, nrEnemy->position.y + nrEnemy->size.y / 2 + dy };

      const bool enemyShootable = canShootToUnit(unit, game, debug, *nrEnemy);

      currentAction.aim = { enemyCenter.x - unit.position.x, enemyCenter.y - (unit.position.y + unit.size.y / 2) };
      currentAction.shoot = enemyShootable;
      if (!enemyShootable && unit.weapon && unit.weapon->magazine < unit.weapon->params.magazineSize / 2) {
         currentAction.reload = true;
      }
   }

   for (const auto& unt : game.units) {
      lastUnitPos[unt.id] = unt.position;
   }

   //std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
   //std::cerr << std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count() << "\n";

   return currentAction;
}
