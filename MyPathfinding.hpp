#include "model/Unit.hpp"
#include "model/Game.hpp"
#include "Debug.hpp"
#include "model/UnitAction.hpp"
#include "Vec2Int.hpp"

#include <optional>
#include <array>

struct PosAndJumpTime final {
   Vec2Int microtilePos;
   int jumptime;
   int usedAt;
   int iteration;

   PosAndJumpTime() : usedAt(-1), iteration(-1), jumptime(-1) {}
   PosAndJumpTime(const Vec2Int& vi, int jumptime, int usedAt, int iteration)
      : microtilePos(vi), jumptime(jumptime), usedAt(usedAt), iteration(iteration) {}

   bool operator==(const PosAndJumpTime& pjt) const {
      return microtilePos == pjt.microtilePos && jumptime == pjt.jumptime;
   }
};

struct PosTimeAction {
   PosAndJumpTime mtPostime;
   UnitAction action;
};

class MyPathfinding {
public:
   MyPathfinding(const Unit& unit, const Game& game, Debug& debug);
   std::optional<UnitAction> firstActionOnPathTo(const Vec2Int& tile, Debug& debug);
   std::optional<UnitAction> firstActionOnPathTo(const Vec2Double& position, Debug& debug);
   int timeTo(const Vec2Double& position, Debug& debug);
private:
   PosTimeAction& parentAt(const PosAndJumpTime& inp);
   PosAndJumpTime& minpAt(const Vec2Int& inp);
};
