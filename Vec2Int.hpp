#pragma once
#include <cstddef>

struct Vec2Int final {
   Vec2Int() : x(0), y(0) {}
   Vec2Int(int x, int y) : x(x), y(y) {}
   int x, y;

   bool operator==(const Vec2Int& vec) const {
      return x == vec.x && y == vec.y;
   }
};

struct Vec2IntHash final {
   std::size_t operator()(const Vec2Int& vec) const {
      return (static_cast<std::size_t>(vec.x) << 32) + static_cast<std::size_t>(vec.y);
   }
};
