#pragma once
#include "model/JumpState.hpp"
#include "model/Vec2Double.hpp"
#include "model/Unit.hpp"

struct MySimplifiedUnit {
   MySimplifiedUnit(const Unit& unit)
      : jumpState(unit.jumpState)
      , position(unit.position)
      , size(unit.size)
      , halfSizeX(unit.halfSizeX) {}

   JumpState jumpState;
   Vec2Double position;
   Vec2Double size;
   double halfSizeX;

   bool intersectsWithPoint(const Vec2Double& point) const {
      return (point.x <= position.x + size.x / 2 && point.x >= position.x - size.x / 2
         && point.y <= position.y + size.y && point.y >= position.y);
   }
};