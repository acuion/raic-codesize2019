cmake_minimum_required(VERSION 3.5)
project(aicup2019)

file(GLOB HEADERS "*.hpp" "model/*.hpp" "csimplesocket/*.h")
SET_SOURCE_FILES_PROPERTIES(${HEADERS} PROPERTIES HEADER_FILE_ONLY TRUE)
file(GLOB SRC "*.cpp" "model/*.cpp" "csimplesocket/*.cpp")
add_executable(aicup2019 ${HEADERS} ${SRC})

# OS and compiler checks.
if(WIN32)
    add_definitions(-DWIN32)
    SET(PROJECT_LIBS Ws2_32.lib)
	set_target_properties(aicup2019 PROPERTIES COMPILE_FLAGS "/arch:AVX2")
else()
    set_target_properties(aicup2019 PROPERTIES COMPILE_FLAGS "-march=haswell -mavx2")
endif()

TARGET_LINK_LIBRARIES(aicup2019 ${PROJECT_LIBS})
set_property(TARGET aicup2019 PROPERTY CXX_STANDARD 17)
