#include "MyUnitActionApply.hpp"
#include "Vec2Int.hpp"

inline static Vec2Int pos2Tiles(const Vec2Double& dv) {
   return Vec2Int(static_cast<int>(dv.x), static_cast<int>(dv.y));
}

inline static bool wall(const Vec2Int& pos, const Game& game) {
   return Tile::WALL == game.level.tiles[pos.x][pos.y];
}

inline static bool platform(const Vec2Int& pos, const Game& game) {
   return Tile::PLATFORM == game.level.tiles[pos.x][pos.y];
}

inline static bool ladder(const Vec2Int& pos, const Game& game) {
   return Tile::LADDER == game.level.tiles[pos.x][pos.y];
}

inline static bool jumpPad(const Vec2Int& pos, const Game& game) {
   return Tile::JUMP_PAD == game.level.tiles[pos.x][pos.y];
}

std::vector<std::vector<char>> wallsPartsum; // y, x
static bool collidesWall(const Vec2Double& position, const Vec2Double& size, const Game& game) {
   const unsigned int x1 = static_cast<unsigned int>(position.x - size.x / 2);
   const unsigned int x2 = static_cast<unsigned int>(position.x + size.x / 2);
   unsigned int y1 = static_cast<unsigned int>(position.y);
   const unsigned int y2 = static_cast<unsigned int>(position.y + size.y);

   for (; y1 <= y2; ++y1) {
      if (wallsPartsum[y1][x2 + 1] - wallsPartsum[y1][x1]) { // x is shifted by 1
         return true;
      }
   }

   return false;
}

static bool onLadder(const Vec2Double& position, const Vec2Double& size, const Game& game) {
   return ladder(pos2Tiles(position), game) || ladder(pos2Tiles(Vec2Double(position.x, position.y + size.y / 2)), game);
}

static bool collidesPlatform(const Vec2Double& position, const Vec2Double& size, const Game& game) {
   const unsigned int x1 = static_cast<unsigned int>(position.x - size.x / 2);
   const unsigned int x2 = static_cast<unsigned int>(position.x + size.x / 2);
   const unsigned int y1 = static_cast<unsigned int>(position.y);
   return platform(Vec2Int(x1, y1), game) || platform(Vec2Int(x2, y1), game);
}

static bool intersectsWithPoint(const Vec2Double& position, const Vec2Double& size, const Vec2Double& point) {
   return (point.x <= position.x + size.x / 2 && point.x >= position.x - size.x / 2
      && point.y <= position.y + size.y && point.y >= position.y);
}

static std::vector<Vec2Double> jumppadsCorners;
static bool collidesJumpPad(const Vec2Double& position, const Vec2Double& size, const Game& game) {
   for (unsigned int i = 0; i < jumppadsCorners.size(); i += 4) {
      if (intersectsWithPoint(position, size, jumppadsCorners[i])
         || intersectsWithPoint(position, size, jumppadsCorners[i + 1])
         || intersectsWithPoint(position, size, jumppadsCorners[i + 2])
         || intersectsWithPoint(position, size, jumppadsCorners[i + 3])) {
         return true;
      }
   }
   return false;
}

inline static void startFreeFall(MySimplifiedUnit& unit, const Game& game) {
   unit.jumpState.canJump = false;
   unit.jumpState.maxTime = 0;
   unit.jumpState.speed = 0;
}

inline static void land(MySimplifiedUnit& unit, const Game& game) {
   unit.jumpState.canJump = true;
   unit.jumpState.canCancel = true;
   unit.jumpState.speed = 0;
   unit.jumpState.maxTime = 0;
}

inline static bool goingUp(const MySimplifiedUnit& unit) {
   return unit.jumpState.speed > 0;
}

// ============================================
const unsigned int microticks = 2;
const unsigned int ticks = 2;
const double pertickFactor = 1 / 60.0;
const double microtickFactor = pertickFactor / microticks;
const double eps = 1e-9;
inline static void flying(MySimplifiedUnit& state, const Game& game, const UnitAction& action) {
   // Not on a ladder, not on a jump pad. Flying up/down
   if (goingUp(state)) { // up
      state.jumpState.maxTime -= microtickFactor;
      if (state.jumpState.canCancel && !action.jump || state.jumpState.maxTime <= 0) { // cancel jump (if allowed) / fall if cannot fly futher
         startFreeFall(state, game);
      }
   }
}

static void jumppading(MySimplifiedUnit& state, const Game& game, const UnitAction& action) {
   // debug.draw(CustomData::Rect(Vec2Float(state.position.x, state.position.y), Vec2Float(0.3, 0.3), ColorFloat(1, 1, 1, 0.2)));
   state.jumpState.canCancel = false;
   state.jumpState.canJump = true;
   state.jumpState.maxTime = game.properties.jumpPadJumpTime;
   state.jumpState.speed = game.properties.jumpPadJumpSpeed;
}

static void laddering(MySimplifiedUnit& state, const Game& game, const UnitAction& action) {
   // debug.draw(CustomData::Rect(Vec2Float(state.position.x, state.position.y), Vec2Float(0.3, 0.3), ColorFloat(1, 1, 1, 0.2)));
   if (action.jump) {
      state.jumpState.canJump = true;
      state.jumpState.maxTime = game.properties.unitJumpTime;
      state.jumpState.speed = game.properties.unitJumpSpeed;
   }
   else {
      if (action.jumpDown) {
         state.jumpState.canJump = false;
         state.jumpState.maxTime = 0;
         state.jumpState.speed = 0;
      }
      else {
         state.jumpState.canJump = true;
         state.jumpState.maxTime = game.properties.unitJumpTime;
         state.jumpState.speed = 0;
      }
   }
}

struct PositionStatus {
   unsigned char mask = 0;

   void hander(MySimplifiedUnit& state, const Game& game, const UnitAction& action) const {
      switch (mask & 3) {
      case 0:
         return flying(state, game, action);
      case 1:
         return laddering(state, game, action);
      case 2:
         return jumppading(state, game, action);
      }
   }
   bool posCollidesWall() const {
      return mask & 4;
   }
   bool posCollidesPlatform() const {
      return mask & 8;
   }
};

static PositionStatus positionInfo[4000][3000];
inline const PositionStatus& infoFor(const MySimplifiedUnit& state) {
   return positionInfo[static_cast<int>(state.position.x * 100)][static_cast<int>(state.position.y * 100)];
}

MySimplifiedUnit applyActionMovements(const MySimplifiedUnit& source, const UnitAction& action, const Game& game, Debug& debug) {
   auto state = source;

   for (unsigned int i = 0; i < microticks * ticks; ++i) {
      infoFor(state).hander(state, game, action);

      if (action.velocity != 0) { // Update X position
         const double prevX = state.position.x;
         state.position.x += action.velocity * microtickFactor;
         if (infoFor(state).posCollidesWall()) {
            const double collx = static_cast<int>(state.position.x) + (action.velocity > 0 ? 1 - eps - state.halfSizeX : eps + state.halfSizeX);
            state.position.x = collx;
         }
      }

      { // Update Y position
         if (!goingUp(state)) { // going down
            const double colly = static_cast<int>(state.position.y) + eps;
            const bool alreadyCollidesPlatform = infoFor(state).posCollidesPlatform();
            state.position.y -= game.properties.unitFallSpeed * microtickFactor;
            const auto currInfo = infoFor(state);
            if (currInfo.posCollidesWall() || !action.jumpDown && !alreadyCollidesPlatform && currInfo.posCollidesPlatform()) {
               state.position.y = colly;
               land(state, game);
               // debug.draw(CustomData::Rect(Vec2Float(state.position.x, state.position.y), Vec2Float(0.3, 0.3), ColorFloat(1, 1, 1, 0.2)));
               if (state.jumpState.canJump && action.jump) {
                  state.jumpState.canJump = true;
                  state.jumpState.canCancel = true;
                  state.jumpState.maxTime = game.properties.unitJumpTime;
                  state.jumpState.speed = game.properties.unitJumpSpeed;
               }
            }
         }
         else { // going up
            const double colly = static_cast<int>(state.position.y) + (1 - eps) - state.size.y;
            state.position.y += state.jumpState.speed * microtickFactor;
            if (infoFor(state).posCollidesWall()) {
               state.position.y = colly;
               startFreeFall(state, game);
            }
         }
      }
   }

   return state;
}

void precomputePathfind(const Game& game) {
   const auto size = game.units[0].size;
   const auto mx = game.level.tiles.size();
   const auto my = game.level.tiles[0].size();
   wallsPartsum.resize(my, std::vector<char>(mx + 1));

   for (unsigned int y = 0; y < my; ++y) {
      for (unsigned int x = 1; x < mx; ++x) {
         wallsPartsum[y][x] = wallsPartsum[y][x - 1] + (Tile::WALL == game.level.tiles[x - 1][y]);
      }
   }

   for (unsigned int x = 0; x < game.level.tiles.size(); ++x) {
      for (unsigned int y = 0; y < game.level.tiles[0].size(); ++y) {
         if (Tile::JUMP_PAD == game.level.tiles[x][y]) {
            jumppadsCorners.emplace_back(x, y);
            jumppadsCorners.emplace_back(x + 1, y);
            jumppadsCorners.emplace_back(x, y + 1);
            jumppadsCorners.emplace_back(x + 1, y + 1);
         }
      }
   }

   unsigned int border = 50;
   for (unsigned int x = border; x < 4000 - border; ++x) {
      for (unsigned int y = border; y < 3000 - border; ++y) {
         const auto position = Vec2Double(x / 100.0, y / 100.0);
         PositionStatus& ps = positionInfo[x][y];
         if (onLadder(position, size, game)) {
            ps.mask |= 1;
         }
         else if (collidesJumpPad(position, size, game)) {
            ps.mask |= 2;
         }
         else {
            ps.mask |= 0; // already 0
         }
         ps.mask |= static_cast<unsigned char>(collidesPlatform(position, size, game)) << 3;
         ps.mask |= static_cast<unsigned char>(collidesWall(position, size, game)) << 2;
      }
   }
}
