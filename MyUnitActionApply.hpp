#pragma once

#include "Debug.hpp"
#include "model/CustomData.hpp"
#include "model/Game.hpp"
#include "model/Unit.hpp"
#include "model/UnitAction.hpp"
#include "MySimplifiedUnit.hpp"

MySimplifiedUnit applyActionMovements(const MySimplifiedUnit& source, const UnitAction& action,
   const Game& game, Debug& debug);
void precomputePathfind(const Game& game);