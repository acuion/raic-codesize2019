#pragma once
#include <ctime>
#include <cmath>
#include "model/Vec2Double.hpp"
#include "model/Vec2Float.hpp"

static const int INF = 10000000; // 1e7

static inline double distanceSqr(const Vec2Double& a, const Vec2Double& b) {
   return (a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y);
}

static inline unsigned int fastrand() {
   static unsigned int g_seed = static_cast<unsigned int>(time(nullptr));
   g_seed = (214013 * g_seed + 2531011);
   return ((g_seed >> 16) & 0x7FFF);
}

static inline Vec2Float vecD2F(const Vec2Double d) {
   return Vec2Float(static_cast<float>(d.x), static_cast<float>(d.y));
}

static double vectorLen(const Vec2Double d) {
   return sqrt(d.x * d.x + d.y * d.y);
}